package com.dtsadnick.dsadnick.capstone;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.concurrent.ExecutionException;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {



    private EditText mUserName;
    private EditText mPassword;
    private Button mLoginButton;
    private Button mRegisterButton;
    private Context mContext;
    private FragmentActivity mFragmentContext;

    public PlaceholderFragment(){}

    /*public  PlaceholderFragment(Context context){
        mContext = context;
    }

    public  PlaceholderFragment(EditText userName, EditText password,
                                Button login, Button register,Context context){
        mUserName = userName;
        mPassword = password;
        mLoginButton = login;
        mRegisterButton = register;
        mContext = context;

    }*/

    @Override
    public void onAttach(Activity activity) {
        mFragmentContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mFragmentContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        mUserName = (EditText) view.findViewById(R.id.userNameInput);
        mPassword = (EditText) view.findViewById(R.id.passwordInput);

        if (PreferenceManager.getDefaultSharedPreferences(mFragmentContext.getApplicationContext())
                .getString("userName", "") != ""){
            mUserName.setText(PreferenceManager.getDefaultSharedPreferences(
                    mFragmentContext.getApplicationContext()).getString("userName",""));
            mPassword.requestFocus();
        }


        mLoginButton = (Button) view.findViewById(R.id.buttonLogin);
        mLoginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                LoginAsync loginAsync = new LoginAsync();

                loginAsync.execute(mUserName.getText().toString(),mPassword.getText().toString());

                try {
                    System.out.println(loginAsync.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                float resultCode = Float.parseFloat(loginAsync.getResponseStr());

                if (resultCode == 1){
                    PreferenceManager.getDefaultSharedPreferences(mFragmentContext.
                            getApplicationContext()).edit().
                            putString("userName", mUserName.getText().toString()).commit();


                   mFragmentContext.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, new MainThread())
                            .addToBackStack(null)
                            .commit();
                } else {
                    Toast.makeText(mFragmentContext.getApplicationContext(), "Login FAILED: " + resultCode, Toast.LENGTH_SHORT).show();
                }
            }
        });


        mRegisterButton = (Button) view.findViewById(R.id.buttonRegister);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FragmentTransaction ft = mFragmentContext.getSupportFragmentManager().beginTransaction();
               ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,android.R.anim.fade_in, android.R.anim.fade_out);

                ft.replace(R.id.container, new RegisterFragment());
                ft.addToBackStack(null);
                ft.commit();

                /*FragmentTransaction fragmentTransaction = mFragmentContext.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

                Fragment registerFragment = new RegisterFragment();

                fragmentTransaction.replace(R.id.container, registerFragment);

                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/

                /*FragmentManager mFragment = mFragmentContext.getSupportFragmentManager();
                mFragment.beginTransaction();
                mFragment.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                mFragment.replace(R.id.container, new RegisterFragment());
                        addToBackStack(null)

                        commit();*/
            }
        });
        return view;
    }
}

