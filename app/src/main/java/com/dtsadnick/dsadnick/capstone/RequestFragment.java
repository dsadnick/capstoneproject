package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by dsadn_000 on 4/23/2015.
 */
public class RequestFragment extends Fragment implements View.OnClickListener {

    private FragmentActivity mFragmentContext;
    private String mUserName;
    private String mGameSelected;
    private String mConsoleSelected;
    private ArrayList<OpenRequests> openRequests;

    private ListView listView;

    public RequestFragment(){}
    /*public RequestFragment(String mUserName){
        mUserName = mUserName;
    }

    public RequestFragment(String game,String console,String username){
        mGameSelected = game;
        mConsoleSelected = console;
        mUserName = username;

    }*/

    @Override
    public void onAttach(Activity activity) {
        mFragmentContext =  (FragmentActivity) activity;
        mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext
                .getApplicationContext()).getString("userName", "");
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_layout, container, false);

        Bundle bundle = new Bundle();
        bundle = getArguments();

        mGameSelected = bundle.getString("mGameSelected");
        mConsoleSelected = bundle.getString("mConsole");
        mUserName = bundle.getString("mUserName");

        listView = (ListView) view.findViewById(R.id.listView2);

        RequestUserOpenGamesAsync requestUserOpenGamesAsync = new RequestUserOpenGamesAsync();

        requestUserOpenGamesAsync.execute(mUserName);



        try {
            System.out.println(requestUserOpenGamesAsync.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        String results = requestUserOpenGamesAsync.getResponseStr();

        System.out.println(results);

        parseJson(requestUserOpenGamesAsync.getResponseStr());

        return view;
    }
    public void parseJson(String jsonString){

        String stringToParse = jsonString;
        openRequests = new ArrayList<>();
        System.out.println("Json String to Parse: " + stringToParse);

        try {

            JSONArray array1 = new JSONArray(stringToParse);
            System.out.println(array1.length() + " : array length");

            for(int i=0;i<array1.length();i++)
            {

                OpenRequests tempGameObject = new OpenRequests();

                JSONObject oneObject = array1.getJSONObject(i);
                String requestId = oneObject.getString("RequestId");
                String requestPlatform = oneObject.getString("RequestPlatform");
                String requestGame = oneObject.getString("RequestGame");
                String requestTime = oneObject.getString("RequestTime");
                String userLooking = oneObject.getString("Users_usersId");

                tempGameObject.setmUID(requestId);
                tempGameObject.setmPlatform(requestPlatform);
                tempGameObject.setmGame(requestGame);
                tempGameObject.setmTime(requestTime);

                System.out.println(tempGameObject);

                openRequests.add(tempGameObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        updateListView();

    }
    public void updateListView(){

        final CustomArrayAdapterRequests adapter = new CustomArrayAdapterRequests(mFragmentContext, openRequests, openRequests.size());
        listView.setAdapter(adapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /*OpenRequests tempGames = openRequests.get(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable("tempGames",tempGames);

                GameDataFragment gameDataFragment = new GameDataFragment();
                gameDataFragment.setArguments(bundle);

                mFragmentContext.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, gameDataFragment)
                        .addToBackStack(null)
                        .commit();*/

                System.out.println("Clicked in requests" +
                        "");
            }


        });



    }
    @Override
    public void onClick(View v) {

    }
}
