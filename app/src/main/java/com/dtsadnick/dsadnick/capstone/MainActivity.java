package com.dtsadnick.dsadnick.capstone;




import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private AdView mAdView;
    private String mUserName;

    public MainActivity(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("userName", "") != ""){
            mUserName = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("userName", "");
        }



        if (savedInstanceState == null) {
            

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();




        }

        mAdView = (AdView) findViewById(R.id.adView);

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        adRequestBuilder.addTestDevice("8259E7974678908A097B3FE63AF2E106");
        adRequestBuilder.addTestDevice("C0FE951BC5058522053A25E4A1852E1A");

        mAdView.loadAd(adRequestBuilder.build());

       setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       if (id == R.id.action_settings) {

           getSupportFragmentManager().beginTransaction()
                   .replace(R.id.container, new SettingsFragment())
                   .addToBackStack(null)
                   .commit();

            return true;
        } else if (id == R.id.action_feedback){
           getSupportFragmentManager().beginTransaction()
                   .replace(R.id.container, new FeedBackFragment())
                   .addToBackStack(null)
                   .commit();
       } else if (id == R.id.action_requests) {

           RequestFragment requestFragment = new RequestFragment();
           Bundle bundle = new Bundle();
           bundle.putString("mUserName",mUserName);
           requestFragment.setArguments(bundle);



          getSupportFragmentManager().beginTransaction()
                   .replace(R.id.container, requestFragment)
                   .addToBackStack(null)
                   .commit();
           /*FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
           fragmentManager.beginTransaction().replace(R.id.container,
                   new RequestFragment(spinnerValue2,mConsole,mUserName))
                   .addToBackStack(null)
                   .commit();*/
       }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        System.out.println("Device Rotated: " + newConfig.toString());


        super.onConfigurationChanged(newConfig);
    }
}

