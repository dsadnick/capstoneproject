package com.dtsadnick.dsadnick.capstone;


import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by dsadn_000 on 4/27/2015.
 */
public class RequestUserOpenGamesAsync extends AsyncTask {


    private String responseStr;
    private HttpResponse response;

    public RequestUserOpenGamesAsync() {
        super();
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        String userScreenName = params[0].toString();

        // Creating HTTP client
        HttpClient httpClient = new DefaultHttpClient();

        // Creating HTTP Post


        HttpPost httpPost = new HttpPost("http://96.41.232.101:60003/scripts/OpenRequestsForUser.php?id=" + userScreenName);

        try {

            response = httpClient.execute(httpPost);

            setResponseStr(EntityUtils.toString(response.getEntity()));

        } catch (ClientProtocolException e) {
            // writing exception to log
            e.printStackTrace();

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

}
