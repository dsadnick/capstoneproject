package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by dsadn_000 on 4/23/2015.
 */
public class SearchFragment extends Fragment implements View.OnClickListener {

    private FragmentActivity mFragmentContext;

    private String mUserName;
    private String mGameSelected;
    private String mConsole;

    private ArrayList<OpenGames> openGames;

    private ListView listView;
    private ListView listView2;
    private Button mRefreshBtn;
    private Button mNewRequestBtn;

    private SearchAsync searchAsync;

    public SearchFragment (){

    }

    /*public SearchFragment(String game, String console){
        mGameSelected = game;
        mConsole = console;
    }*/

    @Override
    public void onAttach(Activity activity) {

        mFragmentContext = (FragmentActivity) activity;

        if (PreferenceManager.getDefaultSharedPreferences(mFragmentContext.getApplicationContext())
                .getString("userName","null") != "null"){
            mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext
                    .getApplicationContext()).getString("userName", "null");
            System.out.println("SavedUserName: " + mUserName);
        }
        super.onAttach(activity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_layout, container, false);

        Bundle bundle = new Bundle();
        bundle = getArguments();

        mGameSelected = bundle.getString("spinnerValue");
        mConsole = bundle.getString("mConsole");

        listView =  (ListView) view.findViewById(R.id.listView);

        mRefreshBtn = (Button) view.findViewById(R.id.refreshbotton);
        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle2 = new Bundle();
                bundle2.putString("spinnerValue", mGameSelected);
                bundle2.putString("mConsole", mConsole);

                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(bundle2);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container,searchFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        mNewRequestBtn = (Button) view.findViewById(R.id.newBtn);
        mNewRequestBtn.setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mFragmentContext)
                        .setTitle("Proceed with request?")
                        .setMessage("Create Request for " + mGameSelected
                                + "?\nRequest will only be good for one hour.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                Bundle bundle = new Bundle();
                                bundle.putString("mGameSelected",mGameSelected);
                                bundle.putString("mConsole",mConsole);
                                bundle.putString("mUserName",mUserName);

                                RequestAsync requestAsync = new RequestAsync();

                                requestAsync.execute(mUserName,mGameSelected,mConsole);



                                System.out.println(bundle.toString()  + ": bndl tstring");

                                RequestFragment requestFragment = new RequestFragment();
                                requestFragment.setArguments(bundle);

                                FragmentManager fragmentManager = getActivity()
                                        .getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.container,
                                        requestFragment)
                                        .addToBackStack(null)
                                        .commit();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                System.out.println("CANCEL  CLICKED");
                            }
                        })
                        .setIcon(android.R.drawable.ic_menu_day)
                        .show();
            }
        });

        searchAsync = new SearchAsync();

        searchAsync.execute(mUserName,mGameSelected,mConsole);




        try {
            System.out.println(searchAsync.get());
        } catch (InterruptedException e) {
            System.out.println(e);
        } catch (ExecutionException e) {
            System.out.println(e);
        }

        parseJson(searchAsync.getResponseStr());



        return view;
    }


    public void parseJson(String jsonString){

        String stringToParse = jsonString;
        openGames = new ArrayList<>();

        try {


            JSONArray array1 = new JSONArray(stringToParse);

            for(int i=0;i<array1.length();i++)
            {
                GetUserDetailsAsysnc getUserDetailsAsysnc = new GetUserDetailsAsysnc();

                OpenGames tempGameObject = new OpenGames();

                JSONObject oneObject = array1.getJSONObject(i);
                String requestId = oneObject.getString("RequestId");
                String requestPlatform = oneObject.getString("RequestPlatform");
                String requestGame = oneObject.getString("RequestGame");
                String requestTime = oneObject.getString("RequestTime");
                String userLooking = oneObject.getString("Users_usersId");


                getUserDetailsAsysnc.execute(userLooking);

                try {
                    System.out.println("Try block: " + getUserDetailsAsysnc.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                String results = getUserDetailsAsysnc.getResponseStr();

                List<String> recievedData = Arrays.asList(results.split(","));

                tempGameObject.setRequestId(requestId);
                tempGameObject.setRequestPlatform(requestPlatform);
                tempGameObject.setRequestGame(requestGame);
                tempGameObject.setRequestTime(requestTime);
                tempGameObject.setUserLooking(recievedData.get(0));
                tempGameObject.setEmail(recievedData.get(1));

                openGames.add(tempGameObject);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        updateListView();

    }

    public void updateListView(){

         CustomArrayAdapter adapter = new
                CustomArrayAdapter(mFragmentContext, openGames, openGames.size());
        listView.setAdapter(adapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                OpenGames tempGames = openGames.get(position);

                if (tempGames == null){
                    System.out.println("TempGames is null for game data fragment");
                }

                Bundle bundle2 = new Bundle();
                //bundle.putSerializable("tempGames",openGames.get(position));
                bundle2.putSerializable("tempGames",tempGames);

                GameDataFragment gameDataFragment = new GameDataFragment();
                gameDataFragment.setArguments(bundle2);

                mFragmentContext.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, gameDataFragment)
                        .addToBackStack(null)
                        .commit();
            }


        });



    }








    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);



    }

    @Override
    public void onClick(View v) {

    }







}
