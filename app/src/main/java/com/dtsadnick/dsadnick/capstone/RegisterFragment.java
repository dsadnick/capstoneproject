package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dsadn_000 on 3/31/2015.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener{

    private EditText mUserFirstName;
    private EditText mUserLastName;
    private EditText mUserScreenName;
    private EditText mUserEmail;
    private EditText mUserPassword;
    private Button mRegisterButton;
    private FragmentActivity mFragmentContext;


    public RegisterFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        mFragmentContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_main, container, false);


        mUserFirstName = (EditText) view.findViewById(R.id.editText);
        mUserLastName = (EditText) view.findViewById(R.id.lastNameText);
        mUserScreenName = (EditText) view.findViewById(R.id.userNameTextInput);
        mUserEmail = (EditText) view.findViewById(R.id.emailTextInput);
        mUserPassword = (EditText)view.findViewById(R.id.passwordInputText);
        mRegisterButton = (Button) view.findViewById(R.id.registerButton);
        mRegisterButton.setOnClickListener(this);

    return view;


    }

    @Override
    public void onClick(View v) {

        mUserFirstName.setError(null);
        mUserLastName.setError(null);
        mUserScreenName.setError(null);
        mUserEmail.setError(null);
        mUserScreenName.setError(null);

        if (mUserFirstName.getText().toString().length() == 0){
            mUserFirstName.setError("First Name Must not be Blank");
        } else if (mUserFirstName.getText().toString().length() == 1){
            mUserFirstName.setError("First Name Must be Greater than 1 letter");
        }
        if (mUserLastName.getText().toString().length() == 0) {
            mUserLastName.setError("Last Name Must not be Blank");
        } else if (mUserLastName.getText().toString().length() == 1){
            mUserLastName.setError("Last Name Must be more than 1 letter");
        }
        if (mUserScreenName.getText().toString().length() == 0){
            mUserScreenName.setError("UserName Must not be Blank");
        } else if (mUserScreenName.getText().toString().length() == 1){
            mUserScreenName.setError("UserName Must be more than 1 letter");
        }
        if(mUserEmail.getText().toString().length() == 0){
            mUserEmail.setError("Email must not be blank");
        } else if(mUserEmail.getText().toString().length() == 1) {
            mUserEmail.setError("Email must be longer than 1");
        } else if (!isEmailAddressValid(mUserEmail.getText().toString())){
            mUserEmail.setError("Email must be a valid email");
        }
        if (mUserPassword.getText().toString().length() == 0){
            mUserPassword.setError("Password Must not be Blank");
        } else if (mUserPassword.getText().toString().length() <= 7){
            mUserPassword.setError("Password Must be between 8 and 10 letters long");
        } else if (mUserPassword.getText().toString().toLowerCase() == "password" ){
            mUserPassword.setError("Password must not be password");
        }

        if (mUserFirstName.getError() == null
                && mUserLastName.getError() == null
                && mUserScreenName.getError() == null
                && mUserEmail.getError() == null
                && mUserScreenName.getError() == null
                && mUserPassword.getError() == null){

            RegisterAsync registerAsync = new RegisterAsync();




            registerAsync.execute(mUserFirstName.getText().toString(),
                    mUserLastName.getText().toString(),
                    mUserScreenName.getText().toString(),
                    mUserEmail.getText().toString(),
                    mUserPassword.getText().toString());


            try {
                System.out.println(registerAsync.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            int resultCode = Integer.parseInt(registerAsync.getResponseStr());

            if (resultCode == 5){

                PreferenceManager.getDefaultSharedPreferences(
                        mFragmentContext.getApplicationContext()).edit()
                        .putString("userName", mUserScreenName.getText().toString()).commit();


                mFragmentContext.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new MainThread())
                        .addToBackStack(null)
                        .commit();
            } else if (resultCode == 1 ||resultCode == 2){
                Toast.makeText(mFragmentContext.getApplicationContext(),
                        "Login FAILED: USERNAME ALL READY IN USE", Toast.LENGTH_LONG).show();
            } else if (resultCode == 3){
                Toast.makeText(mFragmentContext.getApplicationContext(),
                        "Login FAILED: EMAIL ALL READY IN USE", Toast.LENGTH_LONG).show();
            } else if (resultCode == 3){
                Toast.makeText(mFragmentContext.getApplicationContext(),
                        "Login FAILED: EMAIL ALL READY IN USE", Toast.LENGTH_LONG).show();
            } else if (resultCode == 4){
                Toast.makeText(mFragmentContext.getApplicationContext(),
                        "Login FAILED: EMAIL ALL READY IN USE", Toast.LENGTH_LONG).show();
            } else if (resultCode == 5){
                Toast.makeText(mFragmentContext.getApplicationContext(),
                        "Login FAILED: ERROR UNKNOWN\n\n\nPlease report through settings",
                        Toast.LENGTH_LONG).show();
            }

        }



    }

    public static boolean isEmailAddressValid(String email) {

        boolean isEmailValid = false;

        String strExpression = "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+";
        CharSequence inputStr = email;

        Pattern objPattern = Pattern.compile(strExpression , Pattern.CASE_INSENSITIVE);
        Matcher objMatcher = objPattern .matcher(inputStr);
        if (objMatcher.matches()) {
            isEmailValid = true;
        }
        return isEmailValid ;
    }

}

