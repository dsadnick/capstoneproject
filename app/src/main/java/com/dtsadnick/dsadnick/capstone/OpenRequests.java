package com.dtsadnick.dsadnick.capstone;

import java.io.Serializable;

/**
 * Created by dsadn_000 on 4/28/2015.
 */
public class OpenRequests implements Serializable{

    private String mRequestId;
    private String mPlatform;
    private String mTime;
    private String mUID;


    private String mGame;

    public OpenRequests(){

    }
    public OpenRequests(String requestId,String platform, String time, String uId){
        mRequestId = requestId;
        mPlatform = platform;
        mTime = time;
        mUID = uId;
    }

    public String getmRequestId() {
        return mRequestId;
    }

    public void setmRequestId(String mRequestId) {
        this.mRequestId = mRequestId;
    }

    public String getmPlatform() {
        return mPlatform;
    }

    public void setmPlatform(String mPlatform) {
        this.mPlatform = mPlatform;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmUID() {
        return mUID;
    }

    public void setmUID(String mUID) {
        this.mUID = mUID;
    }

    public String getmGame() {
        return mGame;
    }

    public void setmGame(String mGame) {
        this.mGame = mGame;
    }


}
