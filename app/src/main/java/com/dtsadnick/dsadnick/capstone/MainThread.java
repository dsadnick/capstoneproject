package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/**
 * Created by dsadn_000 on 3/31/2015.
 */
public class MainThread extends Fragment implements View.OnClickListener {

    private FragmentActivity mFragmentContext;
    private String mUserName;
    private Spinner mSpinner;
    private Button mSearchButton;

    public MainThread (){

    }
    @Override
    public void onAttach(Activity activity) {

        mFragmentContext = (FragmentActivity) activity;

        if (PreferenceManager.getDefaultSharedPreferences(mFragmentContext.getApplicationContext())
                .getString("userName","null") != "null"){
            mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext
                    .getApplicationContext()).getString("userName", "null");
            System.out.println("SavedUserName: " + mUserName);
        }

        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.second_activity, container, false);
        mFragmentContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        mSpinner = (Spinner) view.findViewById(R.id.spinner);
        mSearchButton = (Button) view.findViewById(R.id.button);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String spinnerValue = mSpinner.getSelectedItem().toString();

                Bundle bundle = new Bundle();
                bundle.putString("spinnerValue",spinnerValue);

                GameFragment tempFragment = new GameFragment();
                tempFragment.setArguments(bundle);

                mFragmentContext.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, tempFragment)
                        .addToBackStack(null)
                        .commit();

            }
        });



        MainThreadAsync mainThreadAsync = new MainThreadAsync();

        mainThreadAsync.execute(mUserName);

        try {
            System.out.println(mainThreadAsync.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        String results = mainThreadAsync.getResponseStr();

        ArrayList<String> consoles;

        consoles = new ArrayList<String>(Arrays.asList(results.split(",")));

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(mFragmentContext,
                android.R.layout.simple_spinner_dropdown_item,consoles);

        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);


        mSpinner.setAdapter(spinnerArrayAdapter);

        return view;
    }


    @Override
    public void onClick(View v) {

    }
}
