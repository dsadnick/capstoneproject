package com.dtsadnick.dsadnick.capstone;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsadn_000 on 4/24/2015.
 */
public class ListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OpenGames> objects;

    public ListAdapter(Context context, ArrayList objects) {
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.listview_layout, parent, false);
            holder = new ViewHolder();
            holder.user = (TextView) convertView.findViewById(R.id.requestUser);
            holder.time = (TextView) convertView.findViewById(R.id.requestTime);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();
/**
       //ClipData.Item item = objects.get(position);
        ArrayList gameses = objects.get(position);
        holder.user.setText(item.ge());
        // Same for description and link*/
        return convertView;
    }

    // Override the other required methods for BaseAdapter

    public class ViewHolder {
        TextView user;
        TextView time;
    }
}