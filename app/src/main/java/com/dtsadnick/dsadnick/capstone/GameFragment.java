package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/**
 * Created by dsadn_000 on 4/23/2015.
 */
public class GameFragment extends Fragment implements View.OnClickListener {

    private Context mFragmentContext;
    private String mUserName;
    private String mConsole;

    private TextView mTextView;

    private Spinner mGameSpinner;
    private Spinner mRequestSpinner;

    private Button mSearchButton;
    private Button mRequestButton;

    private View mView;

    private ArrayList<String> mGames;

    public GameFragment(){}

    /*public GameFragment(String console){

        mConsole = console;
    }*/



    @Override
    public void onAttach(Activity activity) {
        mFragmentContext = activity;
        mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext.getApplicationContext()).getString("userName", "");
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_layout, container, false);
        view.invalidate();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);


        Bundle bundle = getArguments();
        mConsole = bundle.getString("spinnerValue");

        mGameSpinner = (Spinner) view.findViewById(R.id.gamespinner);

        GameFragmentAsync gameFragmentAsync = new GameFragmentAsync();

        gameFragmentAsync.execute(mConsole);

        try {
            System.out.println(gameFragmentAsync.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        String results = gameFragmentAsync.getResponseStr();

        if (results != null){
            mGames = new ArrayList<String>(Arrays.asList(results.split(",")));
        } else {
            mGames = new ArrayList<>();
            mGames.add("test");
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mFragmentContext,
                android.R.layout.simple_spinner_dropdown_item,mGames);
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        mGameSpinner.setAdapter(spinnerArrayAdapter);

        mSearchButton = (Button) view.findViewById(R.id.search2);
        mRequestButton = (Button) view.findViewById(R.id.requestbutton);

        mSearchButton.setOnClickListener(this);

        mRequestButton.setOnClickListener(this);


        mView = view;
        return view;
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.search2:
                v.invalidate();
                String spinnerValue = mGameSpinner.getSelectedItem().toString();
                Bundle bundle = new Bundle();

                bundle.putString("spinnerValue", spinnerValue);
                bundle.putString("mConsole",mConsole);

                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(bundle);


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, searchFragment)
                        .addToBackStack(null)
                        .commit();
                break;


            case R.id.requestbutton :
                final String spinnerValue2 = mGameSpinner.getSelectedItem().toString();
                new AlertDialog.Builder(mFragmentContext)
                        .setTitle("Proceed with request?")
                        .setMessage("Create Request for " + spinnerValue2 + "?\nRequest will only be good for one hour.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                RequestAsync requestAsync = new RequestAsync();

                                requestAsync.execute(mUserName,spinnerValue2,mConsole);

                                String spinnerValue = mGameSpinner.getSelectedItem().toString();

                                Bundle bundle1 = new Bundle();
                                bundle1.putString("mGameSelected",spinnerValue2);
                                bundle1.putString("mConsole",mConsole);
                                bundle1.putString("mUserName",mUserName);

                                RequestFragment requestFragment = new RequestFragment();
                                requestFragment.setArguments(bundle1);

                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.container, requestFragment)
                                        .addToBackStack(null)
                                        .commit();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                System.out.println("CANCEL  CLICKED");
                            }
                        })
                        .setIcon(android.R.drawable.ic_menu_day)
                        .show();
                break;
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("CONFIG CHANGED in GameFragment");
    }
}
