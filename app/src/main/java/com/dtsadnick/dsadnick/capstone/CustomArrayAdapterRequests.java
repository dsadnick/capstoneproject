package com.dtsadnick.dsadnick.capstone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dsadn_000 on 4/28/2015.
 */
public class CustomArrayAdapterRequests extends ArrayAdapter<OpenRequests> {

    private int arraySize;

    public CustomArrayAdapterRequests(Context context, ArrayList<OpenRequests> requests, int size) {
        super(context, 0, requests);
        arraySize = size;
        System.out.print(size + " : -> array size sent to adapter");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        // Get the data item for this position
        OpenRequests openRequests = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.requestlistview_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvRequestId = (TextView) convertView.findViewById(R.id.tv1);
        TextView tvRequestPlatform = (TextView) convertView.findViewById(R.id.tv2);
        TextView tvRequestGame = (TextView) convertView.findViewById(R.id.tv3);
        TextView tvRequestTime =(TextView) convertView.findViewById(R.id.tv4);


        tvRequestId.setText(openRequests.getmUID());
        tvRequestPlatform.setText(openRequests.getmPlatform());
        tvRequestGame.setText(openRequests.getmGame());


        //List<String> tempSplit = openGames.getRequestTime().split(" ");
        List<String> tempSplit = Arrays.asList(openRequests.getmTime().split(" "));
        tvRequestTime.setText(tempSplit.get(1));

        return convertView;
    }

}
