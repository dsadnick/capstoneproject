package com.dtsadnick.dsadnick.capstone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomArrayAdapter extends ArrayAdapter<OpenGames> {


    private int arraySize;

    public CustomArrayAdapter(Context context, ArrayList<OpenGames> games, int size) {
        super(context, 0, games);
        arraySize = size;
        System.out.print(size + " : -> array size sent to adapter");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        // Get the data item for this position
        OpenGames openGames = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvUser = (TextView) convertView.findViewById(R.id.requestUser);
        TextView tvTime = (TextView) convertView.findViewById(R.id.requestTime);
        TextView tvEmail = (TextView) convertView.findViewById(R.id.requestEmail);


        tvUser.setText(openGames.getUserLooking());
        tvEmail.setText(openGames.getEmail());

        //List<String> tempSplit = openGames.getRequestTime().split(" ");
        List<String> tempSplit = Arrays.asList(openGames.getRequestTime().split(" "));

        tvTime.setText(tempSplit.get(1));

        return convertView;
    }



}