package com.dtsadnick.dsadnick.capstone;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by dsadn_000 on 4/24/2015.
 */
public class OpenGames implements Serializable {

    private String requestPlatform;
    private String requestGame;
    private String requestTime;
    private String userLooking;
    private String requestId;
    private String email;

    public OpenGames(){}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequestPlatform() {
        return requestPlatform;
    }

    public void setRequestPlatform(String requestPlatform) {
        this.requestPlatform = requestPlatform;
    }

    public String getRequestGame() {
        return requestGame;
    }

    public void setRequestGame(String requestGame) {
        this.requestGame = requestGame;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getUserLooking() {
        return userLooking;
    }

    public void setUserLooking(String userLooking) {
        this.userLooking = userLooking;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


    public String toString(){

        String outputString = getUserLooking() + "," + getEmail() + "," + requestTime;
        return outputString;
    };


}
