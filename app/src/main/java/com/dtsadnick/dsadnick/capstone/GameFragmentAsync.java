package com.dtsadnick.dsadnick.capstone;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by dsadn_000 on 4/23/2015.
 */
public class GameFragmentAsync extends AsyncTask {

    private String responseStr;
    private HttpResponse response;
    private String consoleSelected;


    public GameFragmentAsync() {
        super();
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }



    @Override
    protected Object doInBackground(Object[] params) {

        consoleSelected = params[0].toString();
        System.out.println(consoleSelected + " in Game Fragment Async");

        // Creating HTTP client
        HttpClient httpClient = new DefaultHttpClient();

        // Creating HTTP Post

        HttpPost httpPost = new HttpPost("http://96.41.232.101:60003/scripts/GamesReturn.php?cn=" + consoleSelected + "");

        try {

            System.out.println(httpClient.toString());

            response = httpClient.execute(httpPost);

            setResponseStr(EntityUtils.toString(response.getEntity()));

        } catch (ClientProtocolException e) {
            // writing exception to log
            e.printStackTrace();

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param o The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
