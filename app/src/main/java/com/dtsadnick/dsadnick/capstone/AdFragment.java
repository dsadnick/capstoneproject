package com.dtsadnick.dsadnick.capstone;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by dsadn_000 on 4/2/2015.
 */
public class AdFragment extends Fragment {

    private AdView mAdView;

    public AdFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.adfragment, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        mAdView = (AdView) view.findViewById(R.id.adView);

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        adRequestBuilder.addTestDevice("8259E7974678908A097B3FE63AF2E106");
        adRequestBuilder.addTestDevice("C0FE951BC5058522053A25E4A1852E1A");
        adRequestBuilder.addTestDevice("EE893638BC178107729C8D73EAB50AAB");

        adRequestBuilder.addKeyword("gaming");
        adRequestBuilder.addKeyword("games");
        mAdView.loadAd(adRequestBuilder.build());

        return view;
    }

    
}
