package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by dsadn_000 on 4/26/2015.
 */
public class GameDataFragment extends Fragment{




    private OpenGames mOpenGames;
    private Context mFragmentContext;
    private String mUserName;

    private TextView mUserLooking;
    private TextView mUserEmail;
    private TextView mGame;
    private TextView mTime;
    private TextView mPlatform;
    private Button mButton;



    public GameDataFragment(){}


    /*public GameDataFragment(OpenGames openGames){
        mOpenGames = openGames;
    }*/
    @Override
    public void onAttach(Activity activity) {
        mFragmentContext = activity;
        mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext
                .getApplicationContext()).getString("userName", "");
       super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gamedata_layout, container, false);

        if (mOpenGames == null){
            mOpenGames = new OpenGames();
        }
        Bundle bundle = getArguments();

        mOpenGames =  (OpenGames) bundle.getSerializable("tempGames");

        mUserLooking = (TextView) view.findViewById(R.id.dataUser);
        mUserEmail = (TextView) view.findViewById(R.id.textView10);
        mGame = (TextView) view.findViewById(R.id.textView12);
        mTime = (TextView) view.findViewById(R.id.textView14);
        mPlatform = (TextView) view.findViewById(R.id.textView16);
        mButton = (Button) view.findViewById(R.id.button3);


        mUserLooking.setText(mOpenGames.getUserLooking());
        mUserEmail.setText(mOpenGames.getEmail());
        mGame.setText(mOpenGames.getRequestGame());
        mTime.setText(mOpenGames.getRequestTime());
        mPlatform.setText(mOpenGames.getRequestPlatform());

        System.out.println("Inside of GameDataFragment");

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{mUserEmail.getText().toString()});
                i.putExtra(Intent.EXTRA_SUBJECT, mUserName + " is looking to play "
                        + mGame.getText().toString() + " with you.");
                i.putExtra(Intent.EXTRA_TEXT   , mUserName + " is looking to play "
                        + mGame.getText().toString() + " with you.  \nEmail them back to get the details\n\n\n\nSENT USING LEET CONNECTION");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(mFragmentContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

}
