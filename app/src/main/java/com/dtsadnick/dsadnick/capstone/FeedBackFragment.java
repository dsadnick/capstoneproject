package com.dtsadnick.dsadnick.capstone;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

/**
 * Created by dsadn_000 on 4/28/2015.
 */
public class FeedBackFragment extends Fragment implements View.OnClickListener {

    private Context mFragmentContext;
    private String mUserName;
    private Button mSubmitButton;
    private EditText mText;

    public FeedBackFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        mFragmentContext = activity;
        mUserName = PreferenceManager.getDefaultSharedPreferences(mFragmentContext.getApplicationContext()).getString("userName", "");
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_layout, container, false);

        mText = (EditText) view.findViewById(R.id.editText2);

        mSubmitButton = (Button) view.findViewById(R.id.button2);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(mText.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {

    }
}

