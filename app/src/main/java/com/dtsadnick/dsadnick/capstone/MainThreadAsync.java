package com.dtsadnick.dsadnick.capstone;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
/**
 * Created by dsadnick on 4/21/2015.
 */
public class MainThreadAsync extends AsyncTask {

    private String responseStr;
    private HttpResponse response;

    public MainThreadAsync() {
        super();
    }


    public String getResponseStr() {
        return responseStr;
    }


    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }



    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Object doInBackground(Object[] params) {

        String username = params[0].toString();
        // Creating HTTP client
        HttpClient httpClient = new DefaultHttpClient();

        // Creating HTTP Post
        HttpPost httpPost = new HttpPost("http://96.41.232.101:60003/scripts/fetchConsole.php");

        try {

            response = httpClient.execute(httpPost);

            setResponseStr(EntityUtils.toString(response.getEntity()));

        } catch (ClientProtocolException e) {
            // writing exception to log
            e.printStackTrace();

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
